﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetTag : MonoBehaviour {
	public Text showTag;
	BoxCollider boxCol;
	// Use this for initialization
	void Start () {
		this.gameObject.tag = "Enemy";
		if (gameObject.GetComponent<Collider> () == null) {
			Debug.Log("Collider added");
			boxCol = gameObject.AddComponent<BoxCollider> ();
		}
		else
		{
			Debug.Log("Collider present");
		}
	}
	
	void Update()
	{
		if (Input.anyKeyDown) {
			showTag.text = "Object tag is : " + tag;
			Debug.Log ("Tag is : " + this.gameObject.tag);
		}
	}
}
